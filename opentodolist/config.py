"""
Provide application configuration.

This module provides means to configure the server
application via appropriate files.
"""


class Config:
    """
    Holds the application configuration.

    This class encapsulates the configuration of the app.
    It provides sane defaults but allows to override any
    options via a configuration file in JSON syntax.

    Optionally, one can pass in a dict-like `config` object
    to programatically specify settings.

    The various configuration options are made available
    as properties of the object. The names of the properties
    equal the names of the configuration objects in the
    configuration file.
    """

    def __init__(self, filename=None, config=None):
        import json

        self._config = config

        if filename is not None:
            with open(filename) as file:
                self._config = json.load(file)

    @property
    def server_name(self):
        """
        The pretty name of the server.

        This is a pretty name for the server. The default
        name is **OpenTodoList** but can be changed by the
        user to something more specific.
        """
        return self._get_prop("server_name", "OpenTodoList")

    @property
    def host(self):
        """
        The host on which the server shall run.

        This property holds the host name (or IP) on which
        the server runs. Defaults to "0.0.0.0".
        """
        return self._get_prop("host", "0.0.0.0")

    @property
    def port(self):
        """
        The port to run the server on.

        This property is only used if the default startup
        mechanism is used. Defaults to 5000 if the debug
        mode is enabled or 8000 for production.
        """
        if self.debug_mode:
            default = 5000
        else:
            default = 8000
        return self._get_prop("port", default)

    @property
    def debug_mode(self):
        """
        Whether or not to run the app in debug mode.

        If debug mode is requested, the default startup
        mechanism will spawn an instance of the built in
        Flask server. If not, gunicorn will be used.

        Defaults to False.
        """
        return self._get_prop("debug_mode", False)

    def _get_prop(self, name, default):
        result = default
        if self._config is not None:
            if name in self._config:
                result = self._config[name]
        return result
