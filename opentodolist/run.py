"""Run the web service."""

from flask import Flask as _Flask
from flask import jsonify as _jsonify


app = _Flask(__name__)


@app.route("/")
def _index():
    return "Hello World"


@app.route("/servercaps/")
def servercaps():
    """
    Provide information about the server.

    This route can be used to retrieve information about the
    server itself. It provides e.g. API version information
    which might be relevant for clients to know before
    attempting sync operations.
    """
    return _jsonify({
        "server_name": "OpenTodoList",
        "v1": True
    })


if __name__ == '__main__':
    app.run()
