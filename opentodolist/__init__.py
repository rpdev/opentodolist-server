"""
OpenTodoList Web.

A Web Service for storing todo lists, notes and more.
"""

__version__ = "0.0.0"

from .config import Config  # noqa
