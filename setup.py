#!/usr/bin/env python3

from distutils.core import setup

import opentodolist as _otl


ARGS = {
    "name": 'OpenTodoList',
    "version": _otl.__version__,
    "description": "Todo list and notes server",
    "author": "Martin Hoeher",
    "author_email": "martin@rpdev.net",
    "url": "https://www.rpdev.net/",
    "packages": ["opentodolist"],
    "install_requires": (
        "Flask-Classy>=0.6.8",
        "Flask-Login>=0.3.0",
        "Flask-Migrate>=2.0.0",
        "Flask-RESTful>=0.3.5",
        "Flask-Script>=2.0.5",
        "Flask-SQLAlchemy>=2.1",
        "Flask>=0.11.1",
        "gunicorn>=19.6.0",
        "nose>=1.3.7",
        "Sphinx>=1.5.2",
    )
}

setup(**ARGS)
