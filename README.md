# A Web Service to Synchronize Todos and Notes

OpenTodoList Web is a web application serving as storage backend for the OpenTodoList desktop and mobile app.

## Health Status

[![build status](https://gitlab.com/rpdev/opentodolist-server/badges/master/build.svg)](https://gitlab.com/rpdev/opentodolist-server/commits/master)
[![coverage report](https://gitlab.com/rpdev/opentodolist-server/badges/master/coverage.svg)](https://gitlab.com/rpdev/opentodolist-server/commits/master)


## Some Background

OpenTodoList has a strong emphasis on ownership of data: Instead of user having to trust companies to handle their precious data well, they shall have ways to keep their data on their own. This is most easily done by strictly keeping data local. However, nowadays you want to have access to your todo lists, notes and other pieces of information regardless on whether you currently work with your laptop, mobile phone, tablet or whatever device you can imagine. So some sort of synchronization is desired.

OpenTodoList Web provides exactly this missing piece: It allows you to host a service with which the OpenTodoList app can synchronize. Consequentially, it is you who decide where to host that application, be it on a trusted web server or even a machine at home.


## The Scope of this Project

OpenTodoList Web is mainly focused on providing a backend storage service for the OpenTodoList app. By itself, it does not expose a user interface, however, this is something that definitely can be added on a later stage.
