"""Tests for the opentodolist.Config class."""

from nose.tools import eq_

from opentodolist import Config


def test_default_config():
    """Test default configuration."""
    cfg = Config()
    eq_(cfg.server_name, "OpenTodoList")
    eq_(cfg.host, "0.0.0.0")
    eq_(cfg.port, 8000)
    eq_(cfg.debug_mode, False)
